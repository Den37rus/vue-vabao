import { mount, shallowMount } from '@vue/test-utils'
import HelloWorld from '@/layouts/SupportPage.vue'

describe('Support page', () => {
  it('render', () => {
    const wrapper = mount(HelloWorld, {})
    expect(wrapper.exists()).toBe(true)
  })
})
