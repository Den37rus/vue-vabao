import { ref, string, bool, object, number, nullable } from 'yup'

export const validators = {
  username: string()
    .min(3, rule => `Минимальная длина ${rule.min} символа`)
    .max(24, rule => `Максимальная длина ${rule.max} символа`)
    .required('Обязательное поле'),

  login: string()
    .min(3, rule => `Минимальная длина ${rule.min} символа`)
    .max(24, rule => `Максимальная длина ${rule.max} символа`)
    .matches(
      /^[a-zA-Z0-9.,/\\%№;:'"-_=+)(*&^%$#@!~)]+$/i,
      'Только латинские буквы,цифры и симолы'
    )
    .required('Обязательное поле'),

  email: string()
    .email('Неверный формат email')
    .max(200, 'Не бывает такого Email`а')
    .required('Обязательное поле'),

  password: string()
    .min(4, rule => `Минимальная длина ${rule.min} символа`)
    .max(64, rule => `Максимальная длина ${rule.max} символа`)
    .matches(
      /^[a-zA-Z0-9.,/\\%№;:'"-_=+)(*&^%$#@!~)]+$/i,
      'Только латинские буквы,цифры и симолы'
    )
    .required('Обязательное поле'),

  passwordConfirm: string()
    .min(4, rule => `Минимальная длина ${rule.min} символа`)
    .max(64, rule => `Максимальная длина ${rule.max} символа`)
    .oneOf([ref('password'), null], 'Пароли не совпадают')
    .matches(
      /^[a-zA-Z0-9.,/\\%№;:'"-_=+)(*&^%$#@!~)]+$/i,
      'Только латинские буквы,цифры и симолы'
    )
    .required('Обязательное поле'),

  rulesConfirm: bool()
    .required('Для регистрации Вам нужно принять условия')
    .isTrue('Для регистрации Вам нужно принять условия'),
  required: string().required('Обязательное поле'),
  select: string().required('Обязательное поле'),
  search: string().required('Обязательное поле'),
  price: number()
    .typeError('Укажите цену')
    .required('Укажите цену')
    .min(10, val => `Минимальная цена ${val.min} рублей`)
    .max(1000000, val => `Максиимальная цена ${val.max} рублей`),
  textarea: string()
    .required('Обязательное поле')
    .max(300, rule => `Максимальная длина описания ${rule.max} символов`),
  captcha: string().required('Пройдите капчу'),
  object
}

export const validationSchema = (...args) => {
  const schemaObject = {}
  for (const field of args) {
    schemaObject[field] = validators[field]
  }
  return object(schemaObject)
}
