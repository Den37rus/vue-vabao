/* eslint-disable no-param-reassign */

import { createStore } from 'vuex'
import headerMenus from './modules/headerMenus.js'
import popupActions from './modules/popupActions.js'
import user from './modules/user.js'
import support from './modules/requestToSupport.js'
import games from './modules/GameModule.js'
import orders from './modules/Orders.js'
import info from './modules/InfoData'

export default createStore({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    headerMenus: {
      namespaced: true,
      ...headerMenus
    },
    popupActions: {
      namespaced: true,
      ...popupActions
    },
    user: {
      namespaced: true,
      ...user
    },
    support: {
      namespaced: true,
      ...support
    },
    games: {
      namespaced: true,
      ...games
    },
    orders: {
      namespaced: true,
      ...orders
    },
    info: {
      namespaced: true,
      ...info
    }
  }
})
