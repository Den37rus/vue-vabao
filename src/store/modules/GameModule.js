/* eslint-disable no-param-reassign */
export default {
  state: () => ({
    oftenGames: []
  }),
  mutations: {
    setGames(state, games) {
      state.oftenGames = games
    }
  },
  actions: {
    async getOftenGames({ commit }) {
      try {
        const { data: gamesList } = await this.axios.get('/api/games/often')
        commit('setGames', gamesList)
      } catch (e) {
        throw e
      }
    }
  },
  getters: {}
}
