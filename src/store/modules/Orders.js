export default {
  state: {
    ordersList: [],
    searchedOrdersCount: 0,
    minPrice: 10,
    maxPrice: 1000
  },
  mutations: {},
  actions: {
    async getOrders({ state }, filters) {
      try {
        const { data } = await this.axios.get(
          `/api/order/filtered-orders?${new URLSearchParams(filters)}`
        )
        state.ordersList = data.orders
        state.searchedOrdersCount = data.count
        return data.orders
      } catch (e) {
        throw e
      }
    },
    async getPriceRange({ state }) {
      try {
        const { data } = await this.axios.get('/api/order/getPriceRange')
        state.minPrice = data.min || 10
        state.maxPrice = data.max || 5000
      } catch (e) {
        throw e
      }
    }
  }
}
