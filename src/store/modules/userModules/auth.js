const parseJwt = token => {
  try {
    return JSON.parse(Buffer.from(token.split('.')[1], 'base64'))
  } catch (e) {
    return null
  }
}

export default {
  state: () => ({
    isLoggedIn: false,
    failedLoginAttempt: 0
  }),
  mutations: {
    loginSuccess(state) {
      state.isLoggedIn = true
      state.failedLoginAttempt = 0
    },
    loginFail(state) {
      state.isLoggedIn = false
      state.failedLoginAttempt += 1
      localStorage.removeItem('authorization')
    },
    logout(state) {
      state.isLoggedIn = false
      localStorage.removeItem(process.env.VUE_APP_STORAGE_FILTERS_KEY)
      localStorage.removeItem('authorization')
      document.cookie = 'login=0'
      this.state.user.id = ''
    }
  },
  actions: {
    logoutAction({ commit }) {
      commit('logout')
      this.dispatch('headerMenus/hideMenus')
    },
    async checkTokenValid({ commit }) {
      const tokenInLocalStorage = localStorage.getItem('authorization')
      if (tokenInLocalStorage) {
        this.state.user.token = tokenInLocalStorage
        const userDataFromToken = parseJwt(tokenInLocalStorage)
        if (userDataFromToken?.id) {
          this.state.user.id = userDataFromToken.id
          if (userDataFromToken.exp * 1000 < Date.now()) {
            try {
              await this.axios.get('/auth/refresh')
            } catch (e) {
              commit('logout')
            }
          }
          commit('loginSuccess')
        } else {
          return Promise.reject()
        }
      } else {
        commit('logout')
      }
    }
  },
  getters: {
    loginAttempts: state => state.failedLoginAttempt,
    isUserAuthenticated: state => state.isLoggedIn
  },
  modules: {}
}
