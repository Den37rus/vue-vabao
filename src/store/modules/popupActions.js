/* eslint-disable no-param-reassign */
function delay(t) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve()
    }, t)
  })
}

export default {
  state: () => ({
    img: null,
    visible: false,
    loaderVisibility: false,
    popupComponentsList: {
      login: 'loginPopup',
      register: 'registerPopup',
      registerSuccess: 'registerSuccessPopup',
      passwordRecovery: 'passRecoveryPopup',
      changePassword: 'changePasswordPopup',
      confirmEmail: 'confirmEmail',
      onModeration: 'AdvOnModeration',
      sell: 'SellPopup',
      search: 'SearchPopup',
      payment: 'PaymentPopup',
      supportDone: 'SuccessSupportRequest'
    },
    popupsChain: [],
    popupComponent: null
  }),
  actions: {
    toggleOverlay({ state }) {
      if (state.loaderVisibility) return
      this.dispatch('headerMenus/hideMenus')
      if (state.popupComponent) {
        state.popupComponent = null
        delay(100).then(() => {
          state.visible = !state.visible
        })
        return
      }
      state.visible = !state.visible
    },
    openPopupComponent({ state, dispatch }, componentName) {
      if (componentName) {
        state.popupsChain.push(componentName)
      }
      dispatch('toggleOverlay')
      delay(100).then(() => {
        state.popupComponent = state.popupsChain.shift()
      })
    },
    appendPopup({ state }, component) {
      state.popupsChain.push(component)
    },
    prependPopup({ state }, nextPopupName) {
      state.popupsChain.unshift(nextPopupName)
    },

    closePopup({ state, dispatch }) {
      const component = state.popupsChain.shift()
      if (!component) {
        dispatch('toggleOverlay')
        return
      }
      state.popupComponent = null
      delay(200).then(() => {
        state.popupComponent = component
      })
    },
    clearPopupsChain({ state, dispatch }) {
      if ((state.loaderVisibility || !state.popupComponent) && !state.img) {
        return
      }
      state.popupsChain = []
      state.img = null
      dispatch('closePopup')
    },
    openPrivatPopup({ state, dispatch }, popupName) {
      dispatch('appendPopup', popupName)
      if (this.state.user.auth.isLoggedIn) {
        dispatch('openPopupComponent')
        return
      }
      dispatch('prependPopup', state.popupComponentsList.login)
      dispatch('openPopupComponent')
    },
    showLoader({ state, dispatch }) {
      dispatch('prependPopup', state.popupComponent)
      state.popupComponent = null
      state.loaderVisibility = true
    },
    showImage({ dispatch, state }, imgUri) {
      state.img = imgUri
      dispatch('toggleOverlay')
    },
    closeLoaderAsResolve({ state, dispatch }) {
      state.loaderVisibility = false
      state.popupsChain.shift()
      dispatch('closePopup')
    },
    closeLoaderAsReject({ state, dispatch }) {
      state.loaderVisibility = false
      dispatch('closePopup')
    }
  },
  getters: {
    getPopupsList: state => state.popupComponentsList,
    getLoaderVisibility: state => state.loaderVisibility
  },
  mutations: {}
}
