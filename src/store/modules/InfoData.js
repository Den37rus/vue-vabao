/* eslint-disable no-param-reassign */
export default {
  state: () => ({
    reviews: [],
    rules: null
  }),
  mutations: {
    setReviews(state, games) {
      state.reviews = games
    },
    setRules(state, rules) {
      state.rules = rules
    }
  },
  actions: {
    async loadReviews({ state, commit }) {
      if (state.reviews.length) return
      try {
        const { data } = await this.axios.get('/api/info/reviews')
        commit('setReviews', data)
      } catch (e) {
        throw e
      }
    },
    async loadRules({ state, commit }) {
      if (state.rules) return
      try {
        const { data } = await this.axios.get('/api/info/rules')
        commit('setRules', data)
      } catch (e) {
        throw e
      }
    }
  }
}
