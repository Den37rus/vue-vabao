/* eslint-disable no-param-reassign */

export default {
  actions: {
    sendForm(_, payload) {
      const [url = '', formFields = {}, files = []] = payload
      const form = new FormData()
      if (files.length) {
        files.forEach(file => {
          form.append('userfiles[]', file, file.name)
        })
      }
      Object.keys(formFields).forEach(key => {
        form.append(key, formFields[key])
      })
      return this.axios.post(`/api/${url}`, form, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      })
    }
  },
  mutations: {}
}
