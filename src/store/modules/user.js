import authModule from './userModules/auth.js'

export default {
  state: () => ({
    stars: 0,
    description: '',
    photo: '/images/user/default.webp',
    name: '',
    login: '',
    id: '',
    createdAt: '',
    lastVisit: '',
    token: '',
    balance: 0,
    transactions: [],
    orders: [],
    rewiews: [],
    isEmailConfirmed: false,
    isEmailNoticeOn: false
  }),
  actions: {
    async getUserData({ commit }, id) {
      if (!id) {
        return this.commit('user/auth/logout')
      }
      try {
        const response = await this.axios.get(`/api/user/${id}`)
        const balance = await this.axios.get(`/api/user/${id}/transactions`)
        commit('setBalance', balance.data.balance)
        return response.data
      } catch (e) {
        throw e
      }
    },
    saveChangedUserProfile(_, formData) {
      return this.axios.patch('/api/user/updateprofile', formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      })
    },
    async getDeals({ state }) {
      const deals = await this.axios.get(`/api/deals/user/${state.id}`)
      return deals.data
    }
  },
  mutations: {
    userDataUpdate(state, payload) {
      Object.keys(payload).forEach(key => {
        state[key] = payload[key]
      })
      state.photo = payload.photo || `/images/user/${payload.id}.webp`
    },
    setToken(state, token) {
      state.token = token
      localStorage.setItem('authorization', token)
    },
    setTransactions(state, payload) {
      const months = [
        'Января',
        'Февраля',
        'Марта',
        'Апреля',
        'Мая',
        'Июня',
        'Июля',
        'Августа',
        'Сентября',
        'Октября',
        'Ноября',
        'Декабря'
      ]
      state.transactions = payload
        .map(el => {
          el.createdAt = {
            date: new Date(el.createdAt).getDate(),
            month: months[new Date(el.createdAt).getMonth()],
            hours: new Date(el.createdAt).getHours(),
            minutes:
              new Date(el.createdAt).getMinutes() < 10
                ? `0${new Date(el.createdAt).getMinutes()}`
                : new Date(el.createdAt).getMinutes(),
            abs: new Date(el.createdAt).getTime()
          }
          return el
        })
        .sort((a, b) => {
          return a.createdAt?.abs - b.createdAt?.abs
        })
    },
    setBalance(state, payload) {
      state.balance = payload
    }
  },
  getters: {
    getUserData(state) {
      return state
    },
    getTransactions(state) {
      return state.transactions
    }
  },
  modules: {
    auth: {
      namespaced: true,
      ...authModule
    }
  }
}
