/* eslint-disable no-param-reassign */
export default {
  state: () => ({
    left: {
      visible: false
    },
    right: {
      visible: false
    },
    menuComponent: null
  }),
  actions: {
    toggleLeftMenu({ state }) {
      state.right.visible = false
      state.left.visible = !state.left.visible
      state.menuComponent = 'leftMenu'
    },
    toggleRightMenu({ state }) {
      state.left.visible = false
      state.right.visible = !state.right.visible
      state.menuComponent = 'rightMenu'
    },
    hideMenus({ state }) {
      state.left.visible = false
      state.right.visible = false
      state.menuComponent = null
    }
  },
  getters: {
    rightMenuVisibility: state => state.right.visible
  }
}
