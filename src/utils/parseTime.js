export const parseTime = date => {
  return {
    abs: date.getTime(),
    date: date.toLocaleDateString('ru-RU', {
      day: 'numeric',
      month: 'long'
    }),
    time: date.toLocaleTimeString('ru-RU', {
      hour: 'numeric',
      minute: '2-digit'
    })
  }
}
