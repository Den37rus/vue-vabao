export const getStorage = key => {
  try {
    return JSON.parse(localStorage.getItem(key))
  } catch (e) {
    console.log(e)
    return null
  }
}

export const setStorage = (key, value) =>
  localStorage.setItem(key, JSON.stringify(value))

export const getFilters = () => {
  const savedFilters = getStorage(process.env.VUE_APP_STORAGE_FILTERS_KEY)
  if (savedFilters && savedFilters.priceRange) {
    savedFilters.priceRange = savedFilters.priceRange.map(el => +el)
    return savedFilters
  }
  return savedFilters
}

export const saveFilters = filters => {
  if (filters == null) {
    setStorage(process.env.VUE_APP_STORAGE_FILTERS_KEY, filters)
    return
  }
  const filtersInStorage = getFilters()
  if (filtersInStorage) {
    setStorage(process.env.VUE_APP_STORAGE_FILTERS_KEY, {
      ...getFilters(),
      ...filters
    })
    return
  }
  setStorage(process.env.VUE_APP_STORAGE_FILTERS_KEY, filters)
}
