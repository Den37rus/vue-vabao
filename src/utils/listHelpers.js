export const categories = {
  account: 'Аккаунты',
  service: 'Услуги',
  item: 'Предметы',
  other: 'Прочее'
}

export const categoriesTextColors = {
  account: 'blue',
  service: 'green',
  item: 'red',
  other: 'gray'
}

export const statuses = {
  progress: 'В процессе',
  open: 'Открыт',
  close: 'Закрыт'
}
