import counterModificator from './counterModificator'
export default function anyTimeAgo(val) {
  let diff = Date.now() - Number(val)

  if (diff < 60 * 1000) {
    return 'Менее минуты'
  }
  if (diff < 60 * 60 * 1000) {
    diff = (diff / 60 / 1000).toFixed()
    return `${diff} ${counterModificator(diff, ['минуту', 'минуты', 'минут'])}`
  }
  if (diff < 24 * 60 * 60 * 1000) {
    diff = (diff / 60 / 60 / 1000).toFixed()
    return `${diff} ${counterModificator(diff, ['час', 'часа', 'часов'])}`
  }
  if (diff < 30 * 24 * 60 * 60 * 1000) {
    diff = (diff / 1000 / 60 / 60 / 24).toFixed()
    return `${diff} ${counterModificator(diff, ['день', 'дня', 'дней'])}`
  }
  diff = (diff / 60 / 60 / 24 / 30 / 1000).toFixed()
  if (diff > 12) return 'Более года'
  return `${diff} ${counterModificator(diff, ['месяц', 'месяца', 'месяцев'])}`
}
