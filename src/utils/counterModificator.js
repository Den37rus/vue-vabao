export default function counterModificator(v, keys) {
  if (v >= 10 && v < 20) return keys[2]
  const valScope = String(v).slice(-1)
  if (valScope === '1') {
    return keys[0]
  }
  if (['2', '3', '4'].includes(valScope)) {
    return keys[1]
  }
  return keys[2]
}
