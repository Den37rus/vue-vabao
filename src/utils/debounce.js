export const debounce = (cb, delay = 1000) => {
  let timer
  return function (...args) {
    clearTimeout(timer)
    timer = setTimeout(cb.bind(null, ...args), delay)
  }
}
