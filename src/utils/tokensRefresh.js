// const parseJwt = token => {
//   try {
//     return JSON.parse(Buffer.from(token.split('.')[1], 'base64'))
//   } catch (e) {
//     return null
//   }
// }

export default async (axios, store) => {
  try {
    const response = await axios.get('/auth/refresh')
    store.commit('user/setToken', response.data.token)
    if (!store.state.user.auth.isLoggedIn) {
      store.commit('user/auth/loginSuccess')
    }
  } catch (e) {
    store.commit('user/auth/loginFail')
    throw new Error(e)
    // console.log(e)
  }
}
