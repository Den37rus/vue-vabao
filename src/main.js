import { createApp } from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import App from './App.vue'
import store from './store'
import createVueRouter from './router'

const app = createApp(App)

app.use(store)
app.use(VueAxios, axios)

app.provide('axios', app.config.globalProperties.axios)

const router = createVueRouter(app)
app.use(router)

app.config.globalProperties.axios.defaults.baseURL =
  process.env.NODE_ENV == 'production'
    ? 'https://den37.site'
    : 'http://localhost:8080'

app.config.globalProperties.axios.interceptors.request.use(
  config => {
    config.headers = {
      ...config.headers,
      Authorization: store.state.user.token
    }
    return config
  },
  error => {
    return Promise.reject(error.response.data.error || error)
  }
)

app.config.globalProperties.axios.interceptors.response.use(
  response => {
    if (response.headers.authorization) {
      store.commit('user/setToken', response.headers.authorization)
    }
    return response
  },
  error => {
    if (error.response.status == 401) {
      store.commit('user/auth/logout')
    }
    return Promise.reject(error.response.data.error || error)
  }
)

store.axios = app.config.globalProperties.axios

store.dispatch('orders/getPriceRange')

app.mount('#app')
