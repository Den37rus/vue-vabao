import { createRouter, createWebHistory } from 'vue-router'
import SupportPage from '@/layouts/SupportPage.vue'
import HomePage from '@/layouts/HomePage.vue'
import RulesPage from '@/layouts/RulesPage.vue'

import store from '../store/index'

const routes = [
  {
    path: '/',
    name: 'root',
    component: HomePage
  },
  {
    name: 'buy-page',
    path: '/buy',
    component: () => import('@/layouts/BuyPage.vue')
  },
  {
    name: 'rules',
    path: '/rules',
    component: RulesPage
  },
  {
    name: 'support',
    path: '/support',
    component: SupportPage
  },
  {
    path: '/order',
    name: 'order-layout',
    component: () => import('@/layouts/WrappedPageToInnerRoute.vue'),
    children: [
      {
        name: 'order',
        path: ':orderId',
        component: () => import('@/components/order/OrderPage.vue')
      }
    ]
  },
  {
    path: '/forbiden',
    name: 'forbiden',
    component: () => import('@/layouts/ForbidenPage.vue')
  },
  {
    path: '/user/:userid',
    component: () => import('@/layouts/WrappedPageToInnerRoute.vue'),
    name: 'UserLayout',

    children: [
      {
        path: 'profile',
        component: () => import('@/components/user/userProfile.vue'),
        name: 'profile'
      },
      {
        path: 'transactions',
        component: () => import('@/components/user/TransactionsPage.vue'),
        name: 'transactions',
        meta: {
          guardian: true
        }
      },
      {
        path: 'history',
        component: () => import('@/components/user/UserHistory.vue'),
        name: 'history',
        meta: {
          guardian: true
        }
      },
      {
        path: 'settings',
        component: () => import('@/components/user/UserSettings.vue'),
        name: 'settings',
        meta: {
          guardian: true
        }
      },
      {
        path: 'settings-edit',
        component: () => import('@/components/user/UserSettingsEdit.vue'),
        name: 'settingsEdit',
        meta: {
          guardian: true
        }
      }
    ]
  }
]

const createVueRouter = app => {
  const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
    scrollBehavior(to, from, savedPosition) {
      if (
        [
          'root',
          'order',
          'profile',
          'profile',
          'settings',
          'settingsEdit'
        ].includes(to.name)
      ) {
        return new Promise(resolve => {
          setTimeout(() => {
            resolve({ top: 0 })
          }, 130)
        })
      }

      if (['buy-page'].includes(to.name) && to.query.page != from.query.page) {
        return { top: 0, behavior: 'smooth' }
      }
    }
  })

  router.beforeEach(async (to, from, next) => {
    store.dispatch('headerMenus/hideMenus')
    if (to.meta.guardian) {
      await store.dispatch('user/auth/checkTokenValid')
      if (store.state.user.auth.isLoggedIn) {
        return next()
      }
      store.dispatch(
        'popupActions/openPopupComponent',
        store.state.popupActions.popupComponentsList.login
      )
      return next({ name: 'root' })
    }
    return next()
  })

  return router
}

export default createVueRouter
