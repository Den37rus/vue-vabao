module.exports = {
  devServer: {
    proxy: {
      '/api/': {
        target: 'http://localhost:3000',
        changeOrigin: true,
        secure: false,
        pathRewrite: { '^/api/': '/api/' }
      },
      '/auth/': {
        target: 'http://localhost:3000',
        changeOrigin: true,
        secure: false,
        pathRewrite: {
          '^/auth/': '/auth/'
        }
      },
      'images/': {
        target: 'http://localhost:3000',
        changeOrigin: true,
        secure: false,
        pathRewrite: {
          '^/images/': '/images/'
        }
      },
      '/docs/': {
        target: 'http://localhost:3000',
        changeOrigin: true,
        secure: false,
        pathRewrite: {
          '^/docs': '/docs/'
        }
      }
    }
  }
  // publicPath:
  //   process.env.NODE_ENV === 'production'
  //     ? `/${process.env.CI_PROJECT_NAME}/`
  //     : '/'
}
